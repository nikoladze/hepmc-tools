#!/usr/bin/env python

import pyhepmc_ng as hep

of2 = hep.WriterAsciiHepMC2("test.hepmc.dat")
of3 = hep.WriterAscii("test.hepmc3.dat")
f = open("test.hepevt")

for l in f:
    h = hep.HEPEVT()
    event_number, n_particles = [int(t) for t in l.strip().split()]
    h.event_number = event_number
    h.nentries = n_particles
    pid = h.pid()
    sta = h.status()
    par = h.parents()
    chi = h.children()
    pm = h.pm()
    v = h.v()
    for i in range(n_particles):
        line = f.readline().strip().split()
        status, pdgid, parent1, parent2, child1, child2 = line[:6]
        px, py, pz, e, m, x, y, z, t = line[6:]
        sta[i] = status
        pid[i] = pdgid
        par[i] = (parent1, parent2)
        chi[i] = (child1, child2)
        pm[i,:4] = (px, py, pz, e)
        pm[i,4] = m
        v[i] = (x, y, z, t)
    evt = hep.GenEvent()
    hep.fill_genevent_from_hepevent_ptr(evt, h.ptr, h.max_size)
    of2.write_event(evt)
    of3.write_event(evt)
