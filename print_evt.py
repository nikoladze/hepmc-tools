#!/usr/bin/env python

import pyhepmc_ng as hep

def print_children(p, ws="", seen_particles=set()):
    if p in seen_particles:
        return ""
    else:
        seen_particles.add(p)
    out = "{}{}\n".format(ws, p.pid)
    for child in p.children:
        out += print_children(child, ws+" ")
    return out

r = hep.ReaderAscii("/home/nikolai/tmp/events.hepmc3")
evt = r.read()

# print(
#     print_children(evt.particles[0])
# )

for p in evt.particles:
    print(p.barcode())
