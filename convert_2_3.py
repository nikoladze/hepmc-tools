#!/usr/bin/env python

import sys
import numpy
import pyhepmc_ng as hep

r = hep.ReaderAsciiHepMC2(sys.argv[1])
w = hep.WriterAscii(sys.argv[2])
while True:
    evt = r.read()
    if len(evt.particles) < 1:
        break
    else:
        w.write(evt)
r.close()
w.close()
